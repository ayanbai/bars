'use strict';
var mongoose = require('mongoose'),
    Contact = mongoose.model('Contact'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {
    return {
        contact: function(req, res, next, id) {
            Contact.load(id, function(err, contact) {
                if (err) return next(err);
                if (!contact) return next(new Error('Failed to load contact ' + id));
                req.contact = contact;
                next();
            });
        },
        create: function(req, res) {
            var contact = new Contact(req.body);
            contact.user = req.user;

            contact.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot save the contact'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/contacts/' + contact._id,
                    name: contact._id
                });

                res.json(contact);
            });
        },
        update: function(req, res) {
            var contact = _.extend(req.contact, req.body);
            contact.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot update the contact'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: contact._id,
                    url: config.hostname + '/contacts/' + contact._id
                });

                res.json(contact);
            });
        },
        destroy: function(req, res) {
            var contact = req.contact;


            contact.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the contact'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: contact._id
                });

                res.json(contact);
            });
        },
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.contact._id,
                url: config.hostname + '/contacts/' + req.contact._id
            });

            res.json(req.contact);
        },
        all: function(req, res) {
            Contact.find({}).sort('-created').populate('user', 'name username').exec(function(err, contacts) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the contacts'
                    });
                }

                res.json(contacts)
            });

        }
    };
};