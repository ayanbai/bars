'use strict';

angular.module('mean.bars').controller('StatusesController', ['$scope', '$stateParams', '$location', 'Global', 'Statuses', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, Global, Statuses, MeanUser, Circles) {
    $scope.global = Global;

    $scope.hasAuthorization = function(status) {
      if (!status || !status.user) return false;
      return MeanUser.isAdmin || status.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
    });

    $scope.create = function(isValid) {
      if (isValid) {
     // $scope.status.permissions = $scope.availableCircles.slice(2);
      console.log('Statuses', Statuses);
        var status = new Statuses($scope.status);

        status.$save(function(response) {
          $location.path('statuses/' + response._id);
        });

        $scope.status = {};

      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(status) {
      if (status) {
        status.$remove(function(response) {
          for (var i in $scope.statuses) {
            if ($scope.statuses[i] === status) {
              $scope.statuses.splice(i, 1);
            }
          }
          $location.path('statuses');
        });
      } else {
        $scope.status.$remove(function(response) {
          $location.path('statuses');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var status = $scope.status;
        if (!status.updated) {
          status.updated = [];
        }
        status.updated.push(new Date().getTime());

        status.$update(function() {
          $location.path('statuses/' + status._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Statuses.query(function(statuses) {
        console.log('statuses ', statuses);
        $scope.statuses = statuses;
      });
    };

    $scope.findOne = function() {
      console.log('$stateParams', $stateParams);
      Statuses.get({
        statusId: $stateParams.statusId
      }, function(status) {
        $scope.status = status;
      });
    };
  }
]);