'use strict';
var mongoose = require('mongoose'),
    Driver = mongoose.model('Driver'),
    Car = mongoose.model('Car'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {
    return {
        driver: function(req, res, next, id) {
            Driver.load(id, function(err, driver) {
                if (err) return next(err);
                if (!driver) return next(new Error('Failed to load driver ' + id));
                req.driver = driver;
                next();
            });
        },
        create: function(req, res) {
            var driver = new Driver(req.body);
            driver.user = req.user;

            console.log('Driver', driver);

            driver.save(function(err, response) {
                if (err) {
                    console.log('Error on set change current car', err);
                    return res.status(500).json({
                        error: 'Cannot save the driver'
                    });
                }
                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/drivers/' + driver._id,
                    name: driver._id
                });
                res.json(driver);
            });
        },
        update: function(req, res) {
            var driver = _.extend(req.driver, req.body);
            driver.save(function(err, response) {
                if (err) {
                    console.log('Error on update drivers car ' + err);
                    return res.status(500).json({
                        error: 'Cannot update the driver'
                    });
                }
                res.json(driver);
            });
        },
        destroy: function(req, res) {
            var driver = req.driver;

            driver.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the driver'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: driver._id
                });

                res.json(driver);
            });
        },
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.driver._id,
                url: config.hostname + '/drivers/' + req.driver._id
            });
            res.json(req.driver);
        },
        all: function(req, res) {
            Driver.find().sort('-created')
            .populate('user', 'name username')
            .exec(function(err, drivers) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the drivers'
                    });
                }
                res.json(drivers);
            });

        }
    };
};