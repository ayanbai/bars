'use strict';
/*global angular, moment, _ */

angular.module('mean.bars').controller('OrdersController', ['$scope', 'MeanUser', 'Circles', '$q', 'Orders', '$uibModal', 'orderResources','yandexLocations',
    function($scope, MeanUser, Circles, $q, Orders, $uibModal, orderResources, yandexLocations) {
        $scope.orders = [];
        $scope.quickFilter = 'current';
        var statusClasses = {
            accepted: 'danger',
            assigned: 'success',
            inplace: 'primary',
            notified: 'warning',
            met: 'info',
            completed: 'default',
            closed: 'default',
            canceled: 'default'
        };

        $scope.service = {
            drunkDriver: 'авто перегон',
            person: 'персона',
            nanny: 'авто няня',
            courier: 'курьерка'
        };

        var update = function() {
            var order = $scope.order;

            if (order.status.key === 'INPLACE') {
                order.moment.inplace = new Date();
            }
            else if (order.status.key === 'NOTIFIED') {
                order.moment.notification = new Date();
            }
            else if (order.status.key === 'MET') {
                order.moment.met = new Date();
            }
            else if (order.status.key === 'COMPLETED') {
                order.moment.end = new Date();
            }
            else if (order.status.key === 'CLOSED') {
                order.moment.closed = new Date();
            }

            if (order.preorder === false) {
                delete order.moment.preorder;
            }

            if (!order.updated) {
                order.updated = [];
            }

            order.updated.push(new Date().getTime());

            order.$update(function() {
                 console.log('order update response ');
                Orders.query({}, function(orders) {
                    console.log('order update Orders.query ');
                    $scope.ordersAll = orders;
                });
            });
        };
        var loadResources = function(state) {
            var deferred = $q.defer();
            orderResources.statuses.then(setStatus, errorCallback).then(function() {
                deferred.resolve();
            });

            function setStatus(statuses) {
                $scope.statuses = statuses;
                $scope.statusKeys = _.map(statuses, function(c) {
                    return c.key;
                });
                $scope.statusCollection = _.reduce(statuses, function(prev, next) {
                    prev[next.key] = next;
                    return prev;
                }, {});
            }

            function errorCallback(error) {
                console.log('Error on get statuses ' + error);
                deferred.reject();
            }

            return deferred.promise;
        };

        function filter(value) {
            if (value === 'all') {
                Orders.query({}, function(orders) {
                    $scope.ordersAll = orders;
                    $scope.orders = orders;
                });

            }
            else if (value === 'current') {
                $scope.orders = _.filter($scope.ordersAll, function(v) {
                    if (v.preorder) {
                        return moment(v.moment.preorder).isBefore(moment().hours(7).minutes(30).add(1, 'd')) && ['CLOSED', 'COMPLETED', 'CANCELED'].indexOf(v.status.key) === -1;
                    }
                    else {
                        return ['CLOSED', 'COMPLETED', 'CANCELED'].indexOf(v.status.key) === -1;
                    }
                });
            }
            else if (value === 'CLOSED') {
                $scope.orders = _.filter($scope.ordersAll, function(v) {
                    return v.status.key === 'CLOSED';
                });
            }
            else if (value === 'COMPLETED') {
                $scope.orders = _.filter($scope.ordersAll, function(v) {
                    return v.status.key === 'COMPLETED';
                });
            }
            else if (value === 'CANCELED') {
                $scope.orders = _.filter($scope.ordersAll, function(v) {
                    return v.status.key === 'CANCELED';
                });
            }
            else if (value === 'preorder') {
                $scope.orders = _.filter($scope.ordersAll, function(v) {
                    return moment(v.moment.preorder).isAfter(moment().hours(7).minutes(30).add(1, 'd')) && ['CLOSED', 'COMPLETED', 'CANCELED'].indexOf(v.status.key) === -1;
                });
            }
            else {
                $scope.orders = $scope.ordersAll;
            }
        }

  
        $scope.getStatusClass = function(key) {
            return 'btn-' + statusClasses[key.toLowerCase()];
        };
        
        $scope.nextStatuses = function(status) {
            var statuses = $scope.statuses;
            var result = [];
            if (status && statuses && $scope.statusKeys) {
                var nextStatus = $scope.statuses[$scope.statusKeys.indexOf(status.key) + 1];

                if (['ACCEPTED', 'ASSIGNED', 'INPLACE', 'NOTIFIED'].indexOf(status.key) >= 0) {
                    result = [nextStatus, $scope.statuses[$scope.statusKeys.indexOf('CANCELED')]];
                }
                else if (status.key === 'MET') {
                    result = [nextStatus, $scope.statuses[$scope.statusKeys.indexOf('CLOSED')]];
                }
                if (status.key === 'COMPLETED') {
                    result = [nextStatus];
                }
                else if (status.key === 'CLOSED') {
                    result = [$scope.statuses[$scope.statusKeys.indexOf('CLOSED')]];
                }
                else if (status.key === 'CANCELED') {
                    result = [$scope.statuses[$scope.statusKeys.indexOf('ACCEPTED')]];
                }
            }
            return result;
        };
        $scope.updateStatus = function(order, status) {
            var nextStatus = status;
            if (['ASSIGNED', 'CLOSED'].indexOf(status.key) !== -1) {
                var modal = $uibModal.open({
                    animation: false,
                    templateUrl: 'bars/views/orders/ordersListStatusModal.html',
                    controller: 'ordersListStatusModalCtrl',
                    size: 'lg',
                    resolve: {
                        order: function() {
                            return order;
                        },
                        resources: function() {
                            return {
                                resources: orderResources,
                                status: nextStatus,
                                yandexLocations: yandexLocations
                            };
                        }
                    }
                });
                modal.result.then(function(updatedItem) {
                    updatedItem.status = nextStatus;
                    $scope.order = updatedItem;
                    update();
                }, function() {
                    console.log('Error on modal save');
                });
            }
            else {
                console.log('update order', order);
                order.status = order.status.key === 'MET' && nextStatus.key === 'CLOSED' ? $scope.statusCollection['CLOSED'] : nextStatus;
                $scope.order = order;
                update();
            }
        };
        $scope.find = function() {
            $scope.initCircles();
            Orders.query({}, function(orders) {
                loadResources().then(function(argument) {
                    $scope.ordersAll = orders;
                    console.log('orders ', orders);
                    $scope.$watch('quickFilter', filter);
                    $scope.quickFilter = 'current';
                });
            }, function(err) {
                console.log('error on load orders', err);
            });
        };
        
        $scope.initCircles = function() {
            Circles.mine(function(acl) {
                $scope.availableCircles = acl.allowed;
                $scope.allDescendants = acl.descendants;
            });
        };
        $scope.hasAuthorization = function(order) {
            if (!order || !order.user) return false;
            return MeanUser.isAdmin || order.user._id === MeanUser.user._id;
        };

        $scope.remove = function(order) {
          if (order) {
            order.$remove(function(response) {
              for (var i in $scope.orders) {
                if ($scope.orders[i] === order) {
                  $scope.orders.splice(i, 1);
                }
              }
              $location.path('orders');
            });
          }
          else {
            $scope.order.$remove(function(response) {
              $location.path('orders');
            });
          }
        };
    }
]);