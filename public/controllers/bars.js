(function () {
  'use strict';

  /* jshint -W098 */
  angular
    .module('mean.bars')
    .controller('BarsController', BarsController);

  BarsController.$inject = ['$scope', 'Global', 'Bars'];

  function BarsController($scope, Global, Bars) {
    $scope.global = Global;
    $scope.package = {
      name: 'bars'
    };
  }
})();