'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CarsSchema = new Schema({
    number: {
        type: String,
        unique: true,
        dropDups: true,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    mileage: Number,
    color: String,
    oils: [{ // Замена масла
        sum: Number,
        date: Date,
        name: String
    }],
    accidents: [{ // ДТП
        date: Date,
        description: String,
        sum: Number
    }],
    fines: [{ // штрафы
        sum: Number,
        date: Date,
        isPaid: Boolean
    }],
    services: [{ // СТО
        sum: Number,
        date: Date
    }],
    insurances: [{ // Страховка
        sum: Number,
        date: Date,
        agent: String
    }]
});

CarsSchema.path('number').validate(function(number) {
    return !!number;
}, 'number cannot be blank');

CarsSchema.path('model').validate(function(model) {
    return !!model;
}, 'model cannot be blank');

CarsSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};



CarsSchema.set('toObject', {
    getters: true
});
CarsSchema.set('toJSON', {
    getters: true
});


CarsSchema.virtual('total').get(function() {
    var car = this;
    var date = new Date();

    function map(v) {
        return v.sum;
    }

    function reduce(prev, next) {
        return prev + next;
    }

    function filter(v) {
        var d = new Date(v.date);
        return d.getMonth() === date.getMonth();
    }

    if (typeof(car.oils) !== 'undefined') {
        var oil = car.oils.filter(filter).map(map).reduce(reduce, 0);
        var accidents = car.accidents.filter(filter).map(map).reduce(reduce, 0);
        var fines = car.fines.filter(filter).map(map).reduce(reduce, 0);
        var services = car.services.filter(filter).map(map).reduce(reduce, 0);
        var insurance = car.insurances.filter(filter).map(map).reduce(reduce, 0);
        var res = oil + accidents + fines + services + insurance;
        
        return res;
    }
    else {
        return 0;
    }
});

mongoose.model('Car', CarsSchema);
