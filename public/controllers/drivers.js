'use strict';
/*global angular, _ */
angular.module('mean.bars').controller('DriversController', ['$scope', '$stateParams', '$location', 'Global', 'Drivers', 'Cars', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, Global, Drivers, Cars, MeanUser, Circles) {
    $scope.global = Global;

    $scope.hasAuthorization = function(driver) {
      if (!driver || !driver.user) return false;
      return MeanUser.isAdmin || driver.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];
    $scope.cars = [];
    $scope.selectedCar = null;
    Circles.mine(function(acl) {
      $scope.availableCircles = acl.allowed;
      $scope.allDescendants = acl.descendants;
    });

    Cars.query(function(cars) {
      $scope.cars = _.filter(cars, function(v) {
        return !v.isBusy;
      });
    });

    $scope.create = function(isValid) {
      if (isValid) {
        var driver = new Drivers($scope.driver);
        if($scope.driver.car && $scope.driver.car.current){
          $scope.driver.car.current = JSON.parse($scope.driver.car.current);
        }
        driver.user = MeanUser.user;
        driver.acl = MeanUser.acl;
        console.log('driver ', driver);
        driver.$save(function(response) {
          console.log('driver car changed', response);
          $scope.driver = {};
          $location.path('drivers/' + response._id);
        });
      }
      else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(driver) {
      if (driver) {
        driver.user = MeanUser.user;
        driver.acl = MeanUser.acl;
        driver.$remove(function(response) {
          for (var i in $scope.drivers) {
            if ($scope.drivers[i] === driver) {
              $scope.drivers.splice(i, 1);
            }
          }
          $location.path('drivers');
        });
      }
      else {
        $scope.driver.$remove(function(response) {
          $location.path('drivers');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var driver = $scope.driver;
        if (!driver.updated) {
          driver.updated = [];
        }
        driver.updated.push(new Date().getTime());

        console.log('$scope.driver.car.current: ', _.isObject($scope.driver.car.current), $scope.driver.car.current);
        console.log('$scope.selectedCar: ',  _.isObject($scope.selectedCar), $scope.selectedCar);

        $scope.driver.car.current = _.isObject($scope.driver.car.current) === false ? JSON.parse($scope.driver.car.current) : $scope.driver.car.current;
        $scope.selectedCar = _.isObject($scope.selectedCar) === false ? JSON.parse($scope.selectedCar) : $scope.selectedCar;

        console.log('$scope.driver.car.current: ', $scope.driver.car.current);
        console.log('$scope.selectedCar: ', $scope.selectedCar);

        $scope.driver.car.used.push({
          date: new Date(),
          car: $scope.driver.car.current
        });
        $scope.driver.car.current = $scope.selectedCar;
        driver.user = MeanUser.user;
        driver.acl = MeanUser.acl;
        if(driver.car){
          driver.car.isBusy = true;
        }
        driver.$update(function() {

          $location.path('drivers/' + driver._id);
        });
      }
      else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Drivers.query(function(drivers) {
        $scope.drivers = drivers;
      });
    };

    $scope.findOne = function() {
      Drivers.get({
        driverId: $stateParams.driverId
      }, function(driver) {
        console.log('driver ', driver);
        $scope.driver = driver;
        $scope.selectedCar = driver.car.current;
      });
    };

    $scope.clearCar = function ($event) {
      $event.stopPropagation();
      $scope.driver.car = null;
    }
  }
]);