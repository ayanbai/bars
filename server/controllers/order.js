'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Order = mongoose.model('Order'),
    request = require('request'),
    config = require('meanio').loadConfig(),
    _ = require('lodash'),
    populations = [{
        path: 'driver',
        select: 'name id car',
        populate: {
            path: 'car',
            select: 'model name number',
            model: 'Car' // модель с нижним регистром не находит
        }
    }, {
        path: 'passengers',
        populate: {
            path: 'contact',
            select: 'name phone'
        }
    }];

module.exports = function(BarsModule) {

    return {
        order: function(req, res, next, id) {
            Order.load(id, function(err, order) {
                // console.log('order', order);
                if (err) return next(err);
                if (!order) return next(new Error('Failed to load order ' + id + err));

                req.order = order;
                next();
            });
        },
        create: function(req, res) {
            var order = new Order(req.body);
            //order.direction.from = mongoose.Types.ObjectId(order.direction.from);
            //order.direction.to = order.direction.to ? mongoose.Types.ObjectId(order.direction.to) : null;
           // order.driver = order.driver ? mongoose.Types.ObjectId(order.driver) : null;
            /*if (order.passengers) {
                order.passengers.map(function(v) {
                    return mongoose.Types.ObjectId(v);
                });
            }*/
            //order.status = mongoose.Types.ObjectId(order.status);
            //order.user = req.user;
            //console.log('server controller user', order.user);
            order.created = new Date();
            order.save(function(err, response) {
                if (err) {
                    console.log('Cannot save the order' + err);
                    return res.status(500).json({
                        error: 'Cannot save the order'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/orders/' + order._id,
                    name: order._id
                });


                res.json(order);
            });
        },
        update: function(req, res) {
            var order = req.order;
            order = _.extend(order, req.body);

            //console.log('order update ', order);

            order.save(function(err) {
                if (err) {
                    console.log('Cannot save the order' + err);
                    return res.status(500).json({
                        error: 'Cannot update the order'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: order._id,
                    url: config.hostname + '/orders/' + order._id
                });

                res.json(order);
            });
        },
        destroy: function(req, res) {
            var order = req.order;


            order.remove(function(err) {
                if (err) {
                    console.log('Cannot save the order' + err);
                    return res.status(500).json({
                        error: 'Cannot delete the order'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: order._id
                });

                res.json(order);
            });
        },
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.order._id,
                url: config.hostname + '/orders/' + req.order._id
            });

            res.json(req.order);
        },
        all: function(req, res) {
            Order.find({}).sort('-created')
                .populate(populations)
                .populate('user', 'name username')
                //.populate('client', 'name title places tarif')
                //.populate('direction.from', 'address sum')
                //.populate('direction.to', 'address sum')
                //.populate('status', 'name key code')
                .exec(function(err, orders) {
                    if (err) {
                        console.log('Cannot list the orders ' + err);
                        return res.status(500).json({
                            error: 'Cannot list the orders'
                        });
                    }
                    res.json(orders);
                });
        },
        reports: function(req, res) {
            var type = req.query.type;

            Order.find({}).sort('created')
                .populate(populations)
                .populate('user', 'name username')
                .populate('client', 'name title places tarif')
                .populate('direction.from', 'address sum')
                .populate('direction.to', 'address sum')
                .populate('status', 'key')
                .exec(function(err, orders) {
                    if (err) {
                        console.log('Cannot list the orders ' + err);
                        return res.status(500).json({
                            error: 'Cannot list the orders'
                        });
                    }
                    res.json(_.filter(orders, function(v) {
                        return v.status.key === 'CLOSED';
                    }));
                });
        },
        locations: function (req, res) {
            var result = null;

            request({
                'method': 'GET',
                'uri': 'http://geocode-maps.yandex.ru/1.x/?format=json&geocode=' +encodeURIComponent(req.query.address)
            }, function (err, response, body) {
                if(err){
                    res.status(500).json({
                        'error': 'Error on load locations'
                    });
                }
                try{
                    result = JSON.parse(body);
                } catch(e){
                    console.log('yandex result ', result);
                }
                res.json(result);
            })

        }
    };
};