'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;
var Bars = new Module('bars');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Bars.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Bars.routes(app, auth, database);
 

  //We are adding a link to the main menu for all authenticated users
  Bars.menus.add({
    title: 'Заказы',
    link: 'all orders',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Клиенты',
    link: 'all clients',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Контакты',
    link: 'all contacts',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Клиенты',
    link: 'all clients',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Водители',
    link: 'all drivers',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Авто',
    link: 'all cars',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });
  Bars.menus.add({
    title: 'Статусы',
    link: 'all statuses',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });

  Bars.menus.add({
    title: 'Новый заказ',
    link: 'create order',
    roles: ['dispatcher', 'finance'],
    menu: 'main'
  });

  Bars.menus.add({
    title: 'Отчёты',
    link: 'reports',
    roles: ['finance'],
    menu: 'main'
  });

  Bars.aggregateAsset('css', 'bars.css');
  Bars.aggregateAsset('js', '../lib/bootstrap-dropdown/index.js');

  Bars.aggregateAsset('js', '../lib/lodash/lodash.js', {
    global: true
  });
  Bars.aggregateAsset('js', '../lib/moment/moment.js', {
    global: true
  });

  //storage.getStorage();

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Bars.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Bars.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Bars.settings(function(err, settings) {
        //you now have the settings object
    });
    */
    



  return Bars;
});