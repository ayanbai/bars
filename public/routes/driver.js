'use strict';
/*global angular*/
angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
var templateUrl = '/bars/views/drivers/';
    $stateProvider
      .state('all drivers', {
        url: '/drivers',
        templateUrl: templateUrl+'list.html'
      })
      .state('create driver', {
        url: '/drivers/create',
        templateUrl: templateUrl+'create.html'
      })
      .state('edit driver', {
        url: '/drivers/:driverId/edit',
        templateUrl: templateUrl+'edit.html'
      })
      .state('driver by id', {
        url: '/drivers/:driverId',
        templateUrl: templateUrl+'view.html'
      });
  }
]);
