'use strict';
/*global angular*/

angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
    var templateUrl = '/bars/views/contacts/';
    $stateProvider
      .state('all contacts', {
        url: '/contacts',
        templateUrl: templateUrl + 'list.html'
      })
      .state('create contact', {
        url: '/contacts/create',
        templateUrl: templateUrl + 'create.html'
      })
      .state('edit contact', {
        url: '/contacts/:contactId/edit',
        templateUrl: templateUrl + 'edit.html'
      })
      .state('contact by id', {
        url: '/contacts/:contactId',
        templateUrl: templateUrl + 'view.html'
      });
  }
]);
