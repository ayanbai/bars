'use strict';
/*global angular, moment, _ */
angular.module('mean.bars').controller('CarsController', ['$scope', '$stateParams', '$location', 'Global', 'Cars', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, Global, Cars, MeanUser, Circles) {
    $scope.global = Global;

    $scope.hasAuthorization = function(car) {
      if (!car || !car.user) return false;
      return MeanUser.isAdmin || car.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
      $scope.availableCircles = acl.allowed;
      $scope.allDescendants = acl.descendants;
    });

    $scope.addOil = function() {
      $scope.car.oils.push({
        name: $scope.oilName,
        sum: $scope.oilSum,
        date: $scope.oilDate
      });
    };

    // accidents
    $scope.addAccident = function() {
      $scope.car.accidents.push({
        description: $scope.accidentDesc,
        sum: $scope.accidentSum,
        date: $scope.accidentDate
      });
    };
    
    // штрафы
    $scope.addFine = function() {
      $scope.car.fines.push({
        sum: $scope.fineSum,
        isPaid: $scope.fineIsPaid,
        date: $scope.finetDate
      });
    };

    // СТО
    $scope.addService = function() {
      $scope.car.services.push({
        sum: $scope.serviceSum,
        date: $scope.serviceDate
      });
    };

    // Страховка
    $scope.addInsurance = function() {
      $scope.car.insurances.push({
        sum: $scope.insuranceSum,
        date: $scope.insuranceDate,
        agent: $scope.insuranceAgent
      });
    };


    $scope.removeCarProp = function(name, index) {
      $scope.car[name].splice(index, 1);
    };


    // создание
    $scope.create = function(isValid) {
      if (isValid) {
        var car = new Cars($scope.car);

        car.user = MeanUser.user;
        car.acl = MeanUser.acl;
        car.$save(function(response) {
          console.log('Created Car', response);
          $scope.car = {};
          $location.path('cars/' + response._id);
        });
      }
      else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(car) {
      if (car) {
        car.$remove(function(response) {
          for (var i in $scope.cars) {
            if ($scope.cars[i] === car) {
              $scope.cars.splice(i, 1);
            }
          }
          $location.path('cars');
        });
      }
      else {
        $scope.car.$remove(function(response) {
          $location.path('cars');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var car = $scope.car;
        if (!car.updated) {
          car.updated = [];
        }
        car.updated.push(new Date().getTime());
        car.user = MeanUser.user;
        car.acl = MeanUser.acl;
        car.$update(function() {
          $location.path('cars/' + car._id);
        });
      }
      else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Cars.query(function(cars) {
        console.log('cars ', cars);
        $scope.cars = cars;
      });
    };

    $scope.findOne = function() {
      Cars.get({
        carId: $stateParams.carId
      }, function(car) {
        $scope.car = car;
      });
    };
  }
]);