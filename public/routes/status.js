'use strict';
/*global angular*/
angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
    var templateUrl = '/bars/views/statuses/';
    // states for my app
    $stateProvider
      .state('all statuses', {
        url: '/statuses',
        templateUrl: templateUrl + 'list.html',
        requiredCircles: {
         circles: ['authenticated']
        }
      })
      .state('create status', {
        url: '/statuses/create',
        templateUrl: templateUrl + 'create.html',
        requiredCircles: {
         circles: ['dispatcher', 'finance'],
          denyState: 'auth.login'
        }
      })
      .state('edit status', {
        url: '/statuses/:statusId/edit',
        templateUrl: templateUrl + 'edit.html',
        requiredCircles: {
         circles: ['dispatcher', 'finance'],
          denyState: 'auth.login'
        }
      })
      .state('status by id', {
        url: '/statuses/:statusId',
        templateUrl: templateUrl + 'view.html',
        requiredCircles: {
         circles: ['dispatcher', 'finance'],
          denyState: 'auth.login'
        }
      });
  }
]);
