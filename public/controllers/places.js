'use strict';

angular.module('mean.bars').controller('PlacesController', ['$scope', '$stateParams', '$location', 'Global', 'Places', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, Global, Places, MeanUser, Circles) {
    $scope.global = Global;

    $scope.hasAuthorization = function(places) {
      if (!places || !places.user) return false;
      return MeanUser.isAdmin || places.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
    });

    $scope.create = function(isValid) {
      if (isValid) {
     // $scope.places.permissions = $scope.availableCircles.slice(2);
      console.log('Places', $scope.place);
        var places = new Places($scope.place);

        places.$save(function(response) {
          $location.path('places/' + response._id);
        });

        $scope.place = {};

      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(places) {
      if (places) {
        places.$remove(function(response) {
          for (var i in $scope.places) {
            if ($scope.places[i] === places) {
              $scope.places.splice(i, 1);
            }
          }
          $location.path('places');
        });
      } else {
        $scope.places.$remove(function(response) {
          $location.path('places');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var place = $scope.place;
        if (!place.updated) {
          place.updated = [];
        }
          place.permissions = $scope.availableCircles.slice(2);
          place.updated.push(new Date().getTime());

        place.$update(function() {
          $location.path('places/' + place._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Places.query(function(places) {
        console.log('places ', places);
        $scope.places = places;
      });
    };

    $scope.findOne = function() {
      Places.get({
        placeId: $stateParams.placeId
      }, function(place) {
        $scope.place = place;
      });
    };
  }
]);