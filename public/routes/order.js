'use strict';
/*global angular*/
angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
var templateUrl = '/bars/views/orders/';
    // states for my app
    $stateProvider
      .state('all orders', {
        url: '/orders',
        templateUrl: templateUrl+'orders.html'
      })
      .state('create order', {
        url: '/orders/create',
        templateUrl: templateUrl+'create.html'
      })
      .state('duplicate order', {
        url: '/orders/:orderId/duplicate',
        templateUrl: templateUrl+'duplicate.html'
      })
      .state('edit order', {
        url: '/orders/:orderId/edit',
        templateUrl: templateUrl+'edit.html'
      })
      .state('order by id', {
        url: '/orders/:orderId',
        templateUrl: templateUrl+'view.html'
      })
      .state('reports', {
        url: '/reports',
        templateUrl: templateUrl+'reports.html'
      });
  }
]);
