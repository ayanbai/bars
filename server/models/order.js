'use strict';
/*global emit*/
var mongoose = require('mongoose'),
Schema = mongoose.Schema,
moment = require("moment"),
_ = require('lodash'),
populations = [{
  path: 'driver',
  populate: {
    path: 'car',
      model: 'Car' // модель с нижним регистром не находит
    }
  }, {
    path: 'passengers',
    populate: {
      path: 'contact'
    }
  }];

  var client = mongoose.model('Client');

  var OrderSchema = new Schema({
    client: {
      type: Object,
      default: null,
      required: true
    },
    passengers: {
      type:Array,
      default: null
    },
    driver: {
      type: Object,
      default: null
    },
    status: {
      type: Object,
      required: true
    },
    moment: {
    // Дата пред заказа
    preorder: {
      type: Date
    },
    // Время подачи по пред заказу
    start: {
      type: Date,
      default: null
    },
    // Время оповещения*
    notification: {
      type: Date,
      default: null
    },
    // Время прибытия
    inplace: {
      type: Date,
      default: null
    },
    // Время встречи*
    met: {
      type: Date,
      default: null
    },

    // Время завершения*
    end: {
      type: Date,
      default: null
    },
    // время закрытия 
    closed: {
      type: Date,
      default: null
    }
  },
  direction: {
    from: {
      type: Object,
      required: true
    },
    to: {
      type: Object,
      default: null
    }
  },
  preorder: {
    type: Boolean,
    default: false
  },
  isCash: {
    type: Boolean,
    default: false
  },
  service: {
    type: String,
    default: null
  },
  sum: {
    type: Number,
    default: 0
  },
  km: {
    type: Number,
    default: 0
  },
  comment: {
    type: String
  },
  ticket: {
    type: Number,
    default: 0
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  updated: {
    type: Array
  },
  created: {
    type: Date,
    default: Date.now
  }
});

OrderSchema.set('toObject', {
  getters: true
});
OrderSchema.set('toJSON', {
  getters: true
});

OrderSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  })
  .populate(populations)
  .populate('user', 'name username')
    // .populate('client')
  //  .populate('direction.from')
  //  .populate('direction.to')
  //  .populate('status')
    .exec(cb);
  };

  function directionSum(name, order) {
    var result = 0;

    if (order.direction) {
      if (order.direction[name]) {
        if (order.isCash === false && order.client && order.client != null && typeof(order.direction[name]._id) !== 'undefined') {
          if (order.client['places'] && order.client['places'].length > 0) {
            var tarif = order.client.places.filter(function(v) {
              var res = typeof(v.place._id !== 'undefined') && String(v.place._id) === String(order.direction[name]._id);
              return res;
            });
            // Тарифная зона действует только для клиентов. 
            //Если для клиента [order.client] адрес [order.direction[name]] указан, тогда это является тарифной зоной.
            result = tarif.length > 0 ? tarif[0].sum : 0;
          }
        }
      }
    }
    return result;
  }

  function momentsIsReady(order) {
    var preorder = moment(order.moment['preorder']),
    notification = moment(order.moment['notification']),
    met = moment(order.moment['met']),
    end = moment(order.moment['end']);

    return _.every([
      (order.preorder ? preorder.isValid() : true),
      notification.isValid(),
      met.isValid(),
      end.isValid()
      ], function(v) {
        return typeof(v) === 'boolean' ? true : v != null && v.valueOf() > 0;
      });
  }

// сумма по тарифной зоне 
// TODO: Прописать в соответсвии с тз или обсудить с заказчиком
OrderSchema.virtual('directionSum').get(function() {
  /*
  var order = this;
  var from = directionSum('from', order);
  var to = directionSum('to', order);
  var res = Math.max(from, to);
  return res;
  */
  return 0;
});

OrderSchema.virtual('waitingTime').get(function() {
  console.log('----Время ожидания----');
  var order = this,
  result = 0;

  if (momentsIsReady(order)) {
    var notification = moment(order.moment['notification']), 
    met = moment(order.moment['met']),
    preorder = moment(order.moment['preorder']);

    if(order.preorder === true){
      console.log('Пред заказа');
      result = 0;
      // время оповещения раньше времени пред заказа
      if(notification.diff(preorder) < 0){
        result = moment.duration(met.diff(preorder)).asMinutes();
        console.log('Машина приехала раньше', result);
      }//время оповещения позже времени пред заказа
      else {
        result = moment.duration(met.diff(notification)).asMinutes();
        console.log('Машина приехала позже', result);
      }
    }else{
      result = moment.duration(met.diff(notification)).asMinutes(); 
      console.log('Текущий заказ', result);
    }

   // console.log('From address', String(order.direction.from.address).indexOf('эропорт'));

   /* if(String(order.direction.from.address).indexOf('эропорт') >=0 ){
      var waitingTimeInAirpot = result - order.client.tarif.waitingTime;
      result = waitingTimeInAirpot < 0 ? 0 : waitingTimeInAirpot;
      console.log('Аэропорт', result);
    }else{
      result = result > 0 && result > 10 ? result - 10 : 0;
      console.log('Минус бесплатное ожидание', result);
    }*/
  }
  //Время ожидания округляется. 
  //Например : 8 мин округляется до 10 мин, 12 мин округляется до 10 мин, 13 мин округляется до 15 мин и т.д.  
  //TODO: Проверить это
  result = (5 * Math.ceil(result / 5))
  console.log('/----Время ожидания----/', result);
  return result;
});

OrderSchema.virtual('tripTime').get(function() {
  
  var order = this,
  result = 0;

  if(!order.isCash && order.client !== null) {
    var met = moment(order.moment['met']),
    end = moment(order.moment['end']);

    if (met.isValid() && end.isValid()) {

      var roundStep = order.client.tarif.roundStep;
      var min = order.client.tarif.min.time;
      var minutes = moment.duration(end.diff(met)).asMinutes();
      // установка минимального заказа
      minutes = minutes < min ? min : minutes;

      
      if (minutes > 30 && minutes <= 60) {
        // округление по шагу указанном в карточке клиента
        result = roundStep * Math.ceil(minutes / roundStep);
      }
      else if (minutes > 60) {
        result = 15 * Math.ceil(minutes / 15);
      }
    }
    else {
      console.log('trip time not ready');
    }
  }
  return result;
});

OrderSchema.virtual('kmSum').get(function(sum) {
  /* var order = this;
  if(!order.isCash){
    return order.km * order.client.tarif.sumPerKm;
  }else{
    return order.km < 5 ? 700 : ((order.km - 5) * 100) + 700;
  }*/
  return 0;
});

OrderSchema.virtual('total').get(function() {
  /*
  var order = this,
  result = 0;
  //console.log('------total-----', order.client.title, order.direction.from.address, order.status.key);
  if(!order.isCash){
    var dSum = order.directionSum;
    if(dSum > 0){
      console.log('Тарифная зона');
      result = order.waitingTime > 0 ? order.directionSum + ((order.waitingTime / 60) * order.client.tarif.sumPerHour) : order.directionSum;

    }else {
      console.log('Расчет суммы по времени поездки');
      //Если время поездки больше или равно 60 минут И км больше 15, тогда сумма поездки будет расчитана по сумме километражу
      if (order.tripTime > 60 && order.km > 15) {
        console.log('Время поездки больше или равно 60 минут и км больше 15');
        result = order.waitingTime > 0 ? order.kmSum + ((order.waitingTime / 60) * order.client.tarif.sumPerHour) : order.kmSum;
      }
      else {//Иначе (Время поездки + время ожидания) * сумма за час в минутах
        var waitingTime = order.waitingTime < 0 ? 0 : order.waitingTime;
        var minutes = waitingTime + order.tripTime;
        console.log('Сумма по времени поездки', order.client);
        result = minutes <= order.client.tarif.min.time ? order.client.tarif.min.sum : (minutes / 60) * order.client.tarif.sumPerHour;
       
     }
  }
}
else{
  result = order.kmSum;
}
 console.log('directionSum ', order.directionSum);
 console.log('tripTime ', order.tripTime);
 console.log('waitingTime ', order.waitingTime);
 console.log('kmSum ', order.kmSum);
 console.log('km ', order.km);
 console.log('total ', result);
 console.log('------/total/-----', order.client.title, order.direction.from.address, order.status.key);

 return Math.ceil(result);
 */

 return 0;
});


mongoose.model('Order', OrderSchema);