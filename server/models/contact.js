'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        unique : true, 
        dropDups: true
    },
    email: {
        type: String
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    updated: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now
    },
    permissions: {
        type: Array
    },

});

/**
 * Statics
 */
ContactSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Contact', ContactSchema);