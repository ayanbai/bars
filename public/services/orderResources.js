(function() {
  /*global angular, _*/
  'use strict';

  angular
    .module('mean.bars')
    .factory('orderResources', Bars);

  Bars.$inject = ['$q', 'MeanUser', 'Clients', 'Places', 'Contacts', 'Statuses', 'Drivers'];

  function Bars($q, MeanUser, Clients, Places, Contacts, Statuses, Drivers) {
    function load(resourse) {
      var deferred = $q.defer();
      resourse.query(function(data) {
        if (data) {
          deferred.resolve(data);
        }
        else {
          deferred.reject();
        }
      });
      return deferred.promise;
    }

    function find(model, param) {
      return $q(function(resolve, reject) {
        model.query(function(places) {
          if (places.length > 0) {
            var res = _.find(places, param);
            if (res) {
              resolve(res);
            }
            else {
              reject();
            }
          }
          else {
            reject();
          }
        });
      });
    }

    return {
      clients: load(Clients),
      places: {
        data: load(Places),
        model: Places,
        transform: function(newAddress) {
          var add = _.upperFirst(newAddress);
          return {
            address: add,
            isTag: true
          };
        },
        save: function(place) {
          return $q(function(resolve, reject) {
            if (place !== null && place.isTag === true) {
              var p = new Places({
                address: place.address,
                user: MeanUser.user
              });
              p.$save(function(response) {
                return resolve(response);
              });
            }
            else {
              return resolve(null);
            }

          });
        }
      },
      contacts: {
        data: load(Contacts),
        model: Contacts,
        transform: function(newTag) {
          var val = newTag.split('-');
          var item = {
            name: _.upperFirst(val[0]),
            phone: val[1]
          };
          return item;
        },
        onSelect: function(contact, cb) {
          return $q(function(resolve, reject) {
            if (contact.isTag) {
              var c = new Contacts({
                name: contact.name,
                phone: contact.phone,
                user: MeanUser.user,
                created: new Date()
              });
              c.$save(function(data) {
                resolve(data);
              });
            }
            else {
              resolve(contact);
            }
          });
        }
      },
      statuses: load(Statuses),
      drivers: load(Drivers)
    };
  }
})();