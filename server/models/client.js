'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ClientsSchema = new Schema({
    title: {
        type: String,
        trim: true,
        default: null
    },
    name: {
        type: String,
        trim: true,
        default: null
    },
    address: { // link to places doc
        type: String,
        trim: true
    },
    places: [{
        sum: {
            type: Number
        },
        place: {
            type: Object
        }
    }],
    tarif: {
        min: {
            time: {
                type: Number,
                default: 30
            },
            sum: {
                type: Number,
                default: 0
            },
            km: {
                type: Number,
                default: 0
            }
        },
        roundStep: {
            type: Number,
            default: 15
        },
        sumPerHour: {
            type: Number,
            default: 0
        },
        sumPerKm: {
            type: Number,
            default: 100
        },
        waitingTime: {
            type: Number,
            default: 0
        }
    },
    services: {
        courier: {
            type: Number,
            default: 0
        },
        nanny: {
            type: Number,
            default: 0
        },
        person: {
            type: Number,
            default: 0
        },
        drunkDriver: {
            type: Number,
            default: 0
        }
    },
    contactPerson: {
        type: String
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    permissions: {
        type: Array
    },
    updated: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now
    }
});

ClientsSchema.set('toObject', {
  getters: true
});
/*
ClientsSchema.path('title').validate(function(title) {
    return !!title;
}, 'Title cannot be blank');
ClientsSchema.path('name').validate(function(name) {
    return !!name;
}, 'Name cannot be blank');
*/
ClientsSchema.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .populate('user', 'name username')
        //.populate('places.place')
        .exec(cb);
};

mongoose.model('Client', ClientsSchema);
