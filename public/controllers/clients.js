'use strict';
/*global angular, _*/
angular.module('mean.bars').controller('ClientsController', ['$scope', '$stateParams', '$location', '$http', 'Global', 'Clients', 'Places', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, $http, Global, Clients, Places, MeanUser, Circles) {
    $scope.global = Global;
    $scope.places = [];
    $scope.isCash = false;
    // карточка физических лиц установлена?
    $scope.individualIsSet = false;
    $scope.selectedPlace = '';
    $scope.selectedPlaceSum = 0;

    $scope.hasAuthorization = function(client) {
      if (!client || !client.user) return false;
      return MeanUser.isAdmin || client.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
      $scope.availableCircles = acl.allowed;
      $scope.allDescendants = acl.descendants;
    });

    $scope.addPlace = function() {

      var hasPlace = _.filter($scope.client.places, function(v) {
        return v.place.address === $scope.selectedPlace;
      });

      if (hasPlace.length === 0 && $scope.selectedPlaceSum > 0) {
        $scope.client.places.push({
          place: {
            address: $scope.selectedPlace
          },
          sum: $scope.selectedPlaceSum
        });
        $scope.selectedPlace = $scope.selectedPlaceSum = null;
      }
    };

    $scope.getLocations = function (val) {
      console.log('getLocations ', val);
        return $http.get('/api/locations', {
          params: {
            address: 'Казахстан, Алматы, '+val
          }
        }).then(function(response){
            var featureMember = response.data.response.GeoObjectCollection.featureMember;
            return _(featureMember).map(function(item){
                return item.GeoObject.name;
              }).uniq().value();
        });
    };

    $scope.removePlace = function(index) {
      console.log('index ', index);
      $scope.client.places.splice(index, 1);
    };

    $scope.create = function(isValid) {
      if (isValid) {
        console.log('client is isValid');

        if($scope.isCash === true){
          $scope.client.title = $scope.client.name = null 
        }

        var client = new Clients($scope.client);
        client.user = MeanUser.user;
        client.acl = MeanUser.acl;

        client.$save(function(response) {
          $location.path('clients/' + response._id);
          $scope.client = {};
        });

      }
      else {
        console.log('client is isValid');
        $scope.submitted = true;
      }
    };

    $scope.remove = function(client) {
      if (client) {
        client.$remove(function(response) {
          for (var i in $scope.clients) {
            if ($scope.clients[i] === client) {
              $scope.clients.splice(i, 1);
            }
          }
          $location.path('clients');
        });
      }
      else {
        $scope.client.$remove(function(response) {
          $location.path('clients');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var client = $scope.client;
        client.user = MeanUser.user;
        client.acl = MeanUser.acl;
        
        if (!client.updated) {
          client.updated = [];
        }
        client.updated.push(new Date().getTime());
        client.$update(function() {
          $location.path('clients/' + client._id);
        });
      }
      else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Clients.query(function(clients) {
        console.log('clients ', clients);
        
        /*
        var individual = _.filter(clients, function(client) {
          return client.title === null && client.name === null;
        });
        $scope.individualIsSet = individual.length == 1;
        */

        $scope.clients = clients;
      });
    };

    $scope.findOne = function() {
      Clients.get({
        clientId: $stateParams.clientId
      }, function(client) {
        $scope.client = client;
        
        if (typeof($scope.client.places) === 'undefined' || $scope.client.places.length === 0) {
          $scope.client.places = [];
        }
        if(client.title === null ){
          $scope.isCash = true;
        }
      });


      Places.query(function(value) {
        $scope.places = value;
      });
    };
  }
]);
