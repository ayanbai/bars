'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Client = mongoose.model('Client'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {

    return {
        /**
         * Find client by id
         */
        client: function(req, res, next, id) {
            Client.load(id, function(err, client) {
                if (err) return next(err);
                if (!client) return next(new Error('Failed to load client ' + id + err));

                req.client = client;
                next();
            });
        },
        /**
         * Create an client
         */
        create: function(req, res) {
            var client = new Client(req.body);
            client.user = req.user;

            client.save(function(err) {
                if (err) {
                    console.log('Error on save Client', err);
                    return res.status(500).json({
                        error: 'Cannot save the client'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/clients/' + client._id,
                    name: client._id
                });

                res.json(client);
            });
        },
        /**
         * Update an client
         */
        update: function(req, res) {
            var client = req.client;

            client = _.extend(client, req.body);
            client.user = req.user;


            client.save(function(err) {
                if (err) {
                    console.error('Cannot update the client' + err);
                    return res.status(500).json({
                        error: 'Cannot update the client'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: client._id,
                    url: config.hostname + '/clients/' + client._id
                });

                res.json(client);
            });
        },
        /**
         * Delete an client
         */
        destroy: function(req, res) {
            var client = req.client;


            client.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the client'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: client._id
                });

                res.json(client);
            });
        },
        /**
         * Show an client
         */
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.client._id,
                url: config.hostname + '/clients/' + req.client._id
            });

            res.json(req.client);
        },
        /**
         * List of BarsModule
         */
        all: function(req, res) {
            // var query = req.acl.query('Client');

            Client.find({})
                .sort('-created').populate('user')
                .exec(function(err, clients) {
                    if (err) {
                        return res.status(500).json({
                            error: 'Cannot list the clients'
                        });
                    }
                    //console.log('All ', clients);
                    res.json(clients);
                });

        }
    };
};