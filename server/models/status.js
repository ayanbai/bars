'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StatusesSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String
    },
    code: {
        type: Number
    },
    key: {
        type: String
    }
});

StatusesSchema.path('name').validate(function(name) {
    return !!name;
}, 'Name cannot be blank');

StatusesSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'user username').exec(cb);
};

mongoose.model('Status', StatusesSchema);
