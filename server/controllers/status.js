'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Status = mongoose.model('Status'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {

    return {
        /**
         * Find status by id
         */
        status: function(req, res, next, id) {
            Status.load(id, function(err, status) {
                ///console.log('status', status);
                if (err) return next(err);
                if (!status) return next(new Error('Failed to load status ' + id+err));
                
                req.status = status;
                next();
            });
        },
        /**
         * Create an status
         */
        create: function(req, res) {
            var status = new Status(req.body);
          //  status.user = req.user;

            status.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot save the status'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/statuses/' + status._id,
                    name: status._id
                });

                res.json(status);
            });
        },
        /**
         * Update an status
         */
        update: function(req, res) {
            var status = req.status;

            status = _.extend(status, req.body);


            status.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot update the status'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: status._id,
                    url: config.hostname + '/statuses/' + status._id
                });

                res.json(status);
            });
        },
        /**
         * Delete an status
         */
        destroy: function(req, res) {
            var status = req.status;


            status.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the status'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: status._id
                });

                res.json(status);
            });
        },
        /**
         * Show an status
         */
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.status._id,
                url: config.hostname + '/statuses/' + req.status._id
            });

            res.json(req.status);
        },
        /**
         * List of BarsModule
         */
        all: function(req, res) {
           // console.log('PlaceQuery ', req.acl);
          //  var query = req.acl.query('Status');

            Status.find({}).sort('-created').populate('user', 'name username').exec(function(err, statuses) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the statuses'
                    });
                }

                res.json(statuses)
            });

        }
    };
}