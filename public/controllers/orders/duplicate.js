'use strict';
/*global angular, moment, _ */

angular.module('mean.bars').controller('OrderDuplicateController', 
  ['$scope', '$stateParams', '$location', 'MeanUser', 'Circles', '$q', 'Orders', 'orderResources',
  function($scope, $stateParams, $location, MeanUser, Circles, $q, Orders, orderResources) {
    var Contacts = null;
    var Places = null;

    var initForm = function() {

      Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
      });

      $scope.order = {
        direction: {
          from: null,
          to: null
        },
        moment: {
          preorder: null,
          start: null,
          notification: null,
          inplace: null,
          met: null,
          end: null,
          closed: null
        },
        preorder: false,
        isCash: false,
        km: 0,
        comment: null,
        driver: null,
        passengers: null,
        status: $scope.statusCollection['ACCEPTED'],
        client: null,
        sum: 0,
        ticket: 0,
        service: {
          courier: false,
          nanny: false,
          person: false,
          drunkDriver: false
        }
      };

      if ($scope.order.hasOwnProperty('driver')) {
        if ($scope.order.hasOwnProperty('status') && $scope.order.status.key === 'ACCEPTED') {
          $scope.order.status = $scope.statusCollection['ASSIGNED'];
        }
      }

      Orders.get({
        orderId: $stateParams.orderId
      }, function(order) {
        $scope.order.direction = [order.direction.to];

        $scope.order.driver = order.driver;
        
        $scope.order.passengers = order.passengers;
        
        if (order.isCash === false && order.hasOwnProperty('client') === true) {
          $scope.order.client = order.client;
        }
        $scope.order.service = order.service;
        $scope.order.client = order.client;
        $scope.order.moment.met = new Date(order.moment.end);
        $scope.order.moment.met.setSeconds(0);
        $scope.order.moment.met.setMilliseconds(0);
        $scope.order.status = $scope.statusCollection['MET'];
      });
    };

    $scope.loadResources = function(state) {
      var deferred = $q.defer();
      orderResources.statuses.then(setStatus, errorCallback('statuses')).then(function() {
        orderResources.places.data.then(successPlacesCallback, errorCallback('places')).then(function() {
          orderResources.contacts.data.then(successContactsCallback, errorCallback('contacts')).then(function() {
            orderResources.drivers.then(successCallback('drivers'), errorCallback('drivers')).then(function() {
              orderResources.clients.then(successCallback('clients'), errorCallback('clients')).then(initForm);
            });
          });
        });
      });

      function successPlacesCallback(result) {
        $scope.places = result;
        Places = orderResources.places.model;
      }

      function successContactsCallback(result) {
        $scope.contacts = result;
        Contacts = orderResources.contacts.model;
      }

      function setStatus(statuses) {
        $scope.statuses = statuses;
        $scope.statusKeys = _.map(statuses, function(c) {
          return c.key;
        });
        $scope.statusCollection = _.reduce(statuses, function(prev, next) {
          prev[next.key] = next;
          return prev;
        }, {});
      }

      function successCallback(name) {
        return function(data) {
          $scope[name] = data;
        };
      }

      function errorCallback(name) {
        return function(error) {
          console.log('Error at ' + name + ' text: ' + error);
          deferred.reject();
        };
      }

      return deferred.promise;
    };

    $scope.hasAuthorization = function(order) {
      if (!order || !order.user) return false;
      return MeanUser.isAdmin || order.user._id === MeanUser.user._id;
    };

    $scope.contactTagTransform = orderResources.contacts.transform;

    $scope.onSelectPassenger = function(select) {
      console.log('onSelectPassenger ', select);
      var res = orderResources.contacts.onSelect(select, function(response) {
        if (response) {
          $scope.contacts.push(response);
        }
      });

      res.then(function(data) {
        console.log('contact selected', data);
        $scope.contacts.push(data);
      }, function(data) {
        console.log('saved new contect', data);
        $scope.contacts.push(data);
      });
    };

    $scope.addressTagTransform = orderResources.places.transform;
    $scope.addressTagClear = orderResources.places.clear;
    $scope.addressTagRefresh = orderResources.places.refresh;

    $scope.onSelectPlace = function(select) {
      console.log('onSelectPlace', select);

      var res = orderResources.places.onSelect(select);

      res.then(function(data) {
        select.selected = data;
      }, function(data) {
        console.log('Error on select or add place', data);
      });
    };

    $scope.create = function(isValid) {

      if ($scope.order.isCash === true) {
        delete $scope.order.client;
      }

      if ($scope.order.preorder === false) {
        delete $scope.order.moment.preorder;
      }

      $scope.order.direction = {
        from: $scope.order.direction[0],
        to: $scope.order.direction[1] || null
      };


      orderResources.places.save($scope.order.direction.from || null).then(function(place) {
        $scope.order.direction.from = place || $scope.order.direction.from;
        orderResources.places.save($scope.order.direction.to || null).then(function(place) {
          $scope.order.direction.to = place || $scope.order.direction.to;

          var order = new Orders($scope.order);
          order.created = new Date();
          order.user = MeanUser.user;
          order.acl = MeanUser.acl;
          console.log('new order ', order);
          order.$save(function(response) {

            $location.path('orders');
          });

        });
      });
    };
  }
]);