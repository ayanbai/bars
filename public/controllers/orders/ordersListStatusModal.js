'use strict';
/*global _ angular */
function controller($scope, $uibModalInstance, order, resources) {

  $scope.order = order;
  $scope.nextStatus = resources.status;
  var yandexLocations = resources.yandexLocations;
  resources = resources.resources;

  resources.places.data.then(successCallback('places')).then(function() {
    resources.drivers.then(successCallback('drivers')).then(function() {
      resources.clients.then(successCallback('clients')).then(function() {
        $scope.selectedDriver = $scope.order.driver;

        $scope.addressTagTransform = resources.places.transform;

        $scope.selectedToAddress = angular.isObject($scope.order.direction.to) ? [$scope.order.direction.to] : [];

        $scope.ticketIsValid = false;
        $scope.kmIsValid = false;
        $scope.sumIsValid = false;
        $scope.selectedToAddressIsValid = false;

        $scope.$watch('order.km', function(newVal) {
          $scope.kmIsValid = newVal > 0;
        });

        $scope.$watch('order.ticket', function(newVal) {
          console.log('ticket ', newVal);
          $scope.ticketIsValid = newVal > 0;
        });

        $scope.$watch('order.sum', function(newVal) {
          $scope.sumIsValid = newVal > 0;
        });

        $scope.$watch('selectedToAddress', function(newVal) {
          console.log('order.direction.to modal', newVal);
          $scope.selectedToAddressIsValid = angular.isObject(newVal) || angular.isArray(newVal);
        });
      });
    });
  });

  $scope.getLocations = function(val) {
    return yandexLocations(val, $scope.order.client.places);
  }

  function successCallback(name) {
    return function(data) {
      $scope[name] = data;
    };
  }

  $scope.okAssingDriver = function(formData) {
    if (formData.$valid) {
      $scope.order.driver = formData.driver.$viewValue;
      console.log('assign driver', $scope.order);
      $uibModalInstance.close($scope.order);
    }
    else {
      $scope.submitted = true;
    }
  };

  $scope.ok = function(formData) {

    var status = [$scope.ticketIsValid, $scope.sumIsValid, $scope.kmIsValid, $scope.selectedToAddressIsValid];

    if (_.some(status)) {
      var to = formData.selectedToAddress || null;
      resources.places.save(to).then(function(place) {
        $scope.order.direction.to = place || to;
        $scope.ticketIsValid = $scope.sumIsValid = $scope.kmIsValid = true;
        console.log('uib-modal ', $scope.order);
        $uibModalInstance.close($scope.order);
      });
    }
    else {
      $scope.ticketIsValid = $scope.sumIsValid = $scope.kmIsValid = false;
      $scope.submitted = true;
    }

  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
}

angular.module('mean.bars').controller('ordersListStatusModalCtrl', controller);