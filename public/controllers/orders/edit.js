'use strict';
/*global angular, moment, _ */

angular.module('mean.bars').controller('OrderEditController', ['$scope', '$stateParams', '$location', 'MeanUser', 'Circles', '$q', 'Orders', 'orderResources',
  function($scope, $stateParams, $location, MeanUser, Circles, $q, Orders, orderResources) {
    var Contacts = null;
    var Places = null;

    function parseDate(value, def) {
      value = moment(value);
      if (value.isValid()) {
        return value.seconds(0).milliseconds(0).toDate();
      }
      else {
        return moment.isDate(def) ? moment(def).seconds(0).milliseconds(0).toDate() : def;
      }
    }

    var initForm = function() {

      Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
      });

      $scope.order = {
        direction: {
          from: null,
          to: null
        },
        moment: {
          preorder: null,
          start: null,
          notification: null,
          inplace: null,
          met: null,
          end: null,
          closed: null
        },
        preorder: false,
        isCash: false,
        km: 0,
        comment: null,
        driver: null,
        passengers: null,
        status: $scope.statusCollection['ACCEPTED'],
        client: null,
        sum: 0,
        ticket: 0,
        service: {
          courier: false,
          nanny: false,
          person: false,
          drunkDriver: false
        }
      };

      $scope.$watch('order.preorder', function(newVal, oldVal) {
        var order = $scope.order;
        if (newVal === true) {
          order.moment.preorder = parseDate(order.moment.preorder, new Date());
        }
        else {
          order.moment.preorder = parseDate(null, null);
        }
      });

      if ($scope.order.hasOwnProperty('driver')) {
        if ($scope.order.hasOwnProperty('status') && $scope.order.status.key === 'ACCEPTED') {
          $scope.order.status = $scope.statusCollection['ASSIGNED'];
        }
      }

      Orders.get({
        orderId: $stateParams.orderId
      }, function(order) {
        _.forEach(order.moment, function(v, k) {
            order.moment[k] = parseDate(v, null);
        });

        order.direction = [order.direction.from, order.direction.to];

        if (order.direction[1] === null || typeof(order.direction[1]) === "undefined") {
          order.direction = [order.direction[0]];
        }

        $scope.order = order;
      });
    };

    $scope.loadResources = function(state) {
      var deferred = $q.defer();
      orderResources.statuses.then(setStatus, errorCallback('statuses')).then(function() {
        orderResources.places.data.then(successPlacesCallback, errorCallback('places')).then(function() {
          orderResources.contacts.data.then(successContactsCallback, errorCallback('contacts')).then(function() {
            orderResources.drivers.then(successCallback('drivers'), errorCallback('drivers')).then(function() {
              orderResources.clients.then(successCallback('clients'), errorCallback('clients')).then(initForm);
            });
          });
        });
      });

      function successPlacesCallback(result) {
        $scope.places = result;
        Places = orderResources.places.model;
      }

      function successContactsCallback(result) {
        $scope.contacts = result;
        Contacts = orderResources.contacts.model;
      }

      function setStatus(statuses) {
        $scope.statuses = statuses;
        $scope.statusKeys = _.map(statuses, function(c) {
          return c.key;
        });
        $scope.statusCollection = _.reduce(statuses, function(prev, next) {
          prev[next.key] = next;
          return prev;
        }, {});
      }

      function successCallback(name) {
        return function(data) {
          $scope[name] = data;
        };
      }

      function errorCallback(name) {
        return function(error) {
          console.log('Error at ' + name + ' text: ' + error);
          deferred.reject();
        };
      }

      return deferred.promise;
    };

    $scope.hasAuthorization = function(order) {
      if (!order || !order.user) return false;
      return MeanUser.isAdmin || order.user._id === MeanUser.user._id;
    };

    $scope.contactTagTransform = orderResources.contacts.transform;

    $scope.onSelectPassenger = function(select) {
      console.log('onSelectPassenger ', select);
      var res = orderResources.contacts.onSelect(select, function(response) {
        if (response) {
          $scope.contacts.push(response);
        }
      });

      res.then(function(data) {
        console.log('contact selected', data);
        $scope.contacts.push(data);
      }, function(data) {
        console.log('saved new contect', data);
        $scope.contacts.push(data);
      });
    };

    $scope.addressTagTransform = orderResources.places.transform;
    $scope.addressTagClear = orderResources.places.clear;
    $scope.addressTagRefresh = orderResources.places.refresh;

    $scope.onSelectPlace = function(select) {
      console.log('onSelectPlace', select);

      var res = orderResources.places.onSelect(select);

      res.then(function(data) {
        select.selected = data;
      }, function(data) {
        console.log('Error on select or add place', data);
      });
    };

    $scope.update = function(isValid) {

      console.log('driver', $scope.order.driver);
      console.log('status', $scope.order.status);

      if (angular.isObject($scope.order.driver) && $scope.order.driver !== null && $scope.order.driver.hasOwnProperty('_id')) {
        if ($scope.order.hasOwnProperty('status') && $scope.order.status.key === 'ACCEPTED') {
          $scope.order.status = $scope.statusCollection['ASSIGNED'];
        }
      }

      if ($scope.order.isCash === true) {
        delete $scope.order.client;
      }

      if ($scope.order.preorder === false) {
        delete $scope.order.moment.preorder;
      }

      $scope.order.user = MeanUser.user;

      $scope.order.direction = {
        from: $scope.order.direction[0],
        to: $scope.order.direction[1] || null
      };

      orderResources.places.save($scope.order.direction.from || null).then(function(place) {
        $scope.order.direction.from = place || $scope.order.direction.from;
        orderResources.places.save($scope.order.direction.to || null).then(function(place) {
          $scope.order.direction.to = place || $scope.order.direction.to;
          $scope.order.user = MeanUser.user;
          $scope.order.acl = MeanUser.acl;
          $scope.order.$update(function(response) {
            console.log("create ", response);
            $location.path('orders');
          });
        });
      });


    };
  }
]);