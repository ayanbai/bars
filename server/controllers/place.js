'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Place = mongoose.model('Place'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {

    return {
        /**
         * Find place by id
         */
        place: function(req, res, next, id) {
            Place.load(id, function(err, place) {
               /// console.log('place', place);
                if (err) return next(err);
                if (!place) return next(new Error('Failed to load place ' + id+err));
                
                req.place = place;
                next();
            });
        },
        /**
         * Create an place
         */
        create: function(req, res) {
            var place = new Place(req.body);
            place.user = req.user;
            //console.log('Create '+ place);
            place.save(function(err) {
                if (err) {
                    console.log('Create Error '+ err);
                    return res.status(500).json({
                        error: 'Cannot save the place'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/places/' + place._id,
                    name: place._id
                });

                res.json(place);
            });
        },
        /**
         * Update an place
         */
        update: function(req, res) {
            var place = req.place;
            place.user = req.user;
            place = _.extend(place, req.body);


            place.save(function(err) {
                if (err) {
                    console.log('Cannot update the place'+ err);
                    return res.status(500).json({
                        error: 'Cannot update the place'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: place._id,
                    url: config.hostname + '/places/' + place._id
                });

                res.json(place);
            });
        },
        /**
         * Delete an place
         */
        destroy: function(req, res) {
            var place = req.place;


            place.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the place'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: place._id
                });

                res.json(place);
            });
        },
        /**
         * Show an place
         */
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.place._id,
                url: config.hostname + '/places/' + req.place._id
            });

            res.json(req.place);
        },
        /**
         * List of BarsModule
         */
        all: function(req, res) {
           // console.log('PlaceQuery ', req.acl);
           // var query = req.acl.query('Place');

            Place.find({}).sort('-created').populate('user', 'name username').exec(function(err, places) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the places'
                    });
                }

                res.json(places)
            });

        }
    };
}