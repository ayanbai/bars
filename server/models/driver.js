'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Drivers Schema
 */
var DriversSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        dropDups: true
    },
    id: {
        type: Number,
        required: true,
        unique: true,
        dropDups: true
    },
    car: {
            current: {
                type: Object,
                default: null
            },
            used: [
                {
                    data: Date,
                    car: {
                        type: Object,
                        default: null
                    }
                }
            ]
        },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    updated: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now
    },
    fired: {
        type: Date,
        default: null
    }
});


/**
 * Validations
 */
DriversSchema.path('name').validate(function(name) {
    return !!name;
}, 'Name cannot be blank');

DriversSchema.path('id').validate(function(id) {
    return !!id;
}, 'Id cannot be blank');

/**
 * Statics
 */
DriversSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};

mongoose.model('Driver', DriversSchema);
