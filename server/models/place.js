'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Places Schema
 */
var PlacesSchema = new Schema({
    address: {
        type: String,
        required: true,
        trim: true,
        unique : true, 
        dropDups: true
    },
    sum: {
        type: Number
    },
    coordinates: {
        type: String,
        trim: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    permissions: {
        type: Array
    },
    updated: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now
    }
});

/**
 * Validations
 */

PlacesSchema.path('address').validate(function(address) {
    return !!address;
}, 'Address cannot be blank');

/**
 * Statics
 */
PlacesSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name username').exec(cb);
};


mongoose.model('Place', PlacesSchema);
