'use strict';
/*global angular*/

angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
var templateUrl = '/bars/views/cars/';
    $stateProvider
      .state('all cars', {
        url: '/cars',
        templateUrl: templateUrl+'list.html'
      })
      .state('create car', {
        url: '/cars/create',
        templateUrl: templateUrl+'create.html'
      })
      .state('edit car', {
        url: '/cars/:carId/edit',
        templateUrl: templateUrl+'edit.html'
      })
      .state('car by id', {
        url: '/cars/:carId',
        templateUrl: templateUrl+'view.html'
      });
  }
]);
