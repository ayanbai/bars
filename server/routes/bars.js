'use strict';

// Contact authorization helpers
var hasAuthorization = function(name) {
  return function(req, res, next) {
    if (!req.user.isAdmin && !req[name].user._id.equals(req.user._id)) {
      return res.status(401).send('User is not authorized');
    }
    next();
  };
};

var hasPermissions = function(req, res, next) {
  req.body.permissions = req.body.permissions || ['authenticated'];

  for (var i = 0; i < req.body.permissions.length; i++) {
    var permission = req.body.permissions[i];
    if(typeof(req.acl) === 'undefined'){
      console.log('req.acl: ', req.acl, req.body.acl);
      req.acl = {
        user: {
          allowed: req.body.acl.allowed
        }
      }
    }
    if (req.acl.user.allowed.indexOf(permission) === -1) {
      return res.status(401).send('User not allowed to assign ' + permission + ' permission.');
    }
  }
  next();
};
/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Bars, app, auth, database) {

  function rout(name, e) {
    if (typeof(e) === 'undefined') {
      e = '';
    }

    var controller = require('../controllers/' + name)(Bars, database);
    app.route('/api/' + name + e + 's')
      .get(controller.all)
      .post(auth.requiresLogin, hasPermissions, controller.create);
    app.route('/api/' + name + e + 's/:' + name + 'Id')
      .get(auth.isMongoId, controller.show)
      .post(auth.requiresLogin, hasPermissions, controller.create)
      .put(auth.isMongoId, auth.requiresLogin, hasAuthorization(name), hasPermissions, controller.update)
      .delete(auth.isMongoId, auth.requiresLogin, hasAuthorization(name), hasPermissions, controller.destroy);  

    app.param(name + 'Id', controller[name]);
    
    if(name === 'order'){
      app.route('/api/reports').get(controller.reports);
      app.route('/api/locations').get(controller.locations);
    }
  }

  rout('client');
  rout('contact');
  rout('driver');
  rout('place');
  rout('status', 'e');
  rout('order');
  rout('car');
  
 

};
