'use strict';
var mongoose = require('mongoose'),
    Car = mongoose.model('Car'),
    config = require('meanio').loadConfig(),
    _ = require('lodash');

module.exports = function(BarsModule) {
    return {
        car: function(req, res, next, id) {
            Car.load(id, function(err, car) {
                if (err) return next(err);
                if (!car) return next(new Error('Failed to load car ' + id));
                req.car = car;
                next();
            });
        },
        create: function(req, res) {
            var car = new Car(req.body);
            car.user = req.user;

            car.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot save the car'
                    });
                }

                BarsModule.events.publish({
                    action: 'created',
                    user: {
                        name: req.user.name
                    },
                    url: config.hostname + '/cars/' + car._id,
                    name: car._id
                });

                res.json(car);
            });
        },
        update: function(req, res) {
            var car = _.extend(req.car, req.body);
            car.save(function(err) {
                if (err) {
                    console.log('update error: ', err);
                    return res.status(500).json({
                        error: 'Cannot update the car'
                    });
                }

                BarsModule.events.publish({
                    action: 'updated',
                    user: {
                        name: req.user.name
                    },
                    name: car._id,
                    url: config.hostname + '/cars/' + car._id
                });

                res.json(car);
            });
        },
        destroy: function(req, res) {
            var car = req.car;


            car.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the car'
                    });
                }

                BarsModule.events.publish({
                    action: 'deleted',
                    user: {
                        name: req.user.name
                    },
                    name: car._id
                });

                res.json(car);
            });
        },
        show: function(req, res) {

            BarsModule.events.publish({
                action: 'viewed',
                user: {
                    name: req.user.name
                },
                name: req.car._id,
                url: config.hostname + '/cars/' + req.car._id
            });
            
            console.log('show car ', req.car);

            res.json(req.car);
        },
        all: function(req, res) {
            Car.find({}).sort('-created').populate('user', 'name username').exec(function(err, cars) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the cars'
                    });
                }

                res.json(cars);
            });

        }
    };
};