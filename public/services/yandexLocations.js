(function() {
  /*global angular, _*/
  'use strict';

  angular
    .module('mean.bars')
    .factory('yandexLocations', Bars);

  Bars.$inject = ['$q','$http'];

  function Bars($q, $http) {
    return function (val, tarifZones) {
          return $http.get('/api/locations', {
            params: {
              address: 'Казахстан, Алматы, '+val
            }
          }).then(function(response){
              var featureMember = response.data.response.GeoObjectCollection.featureMember;
              featureMember = _(featureMember).map(function(item){
                  return {
                      place: {
                          address: item.GeoObject.name,
                          type: 'Адрес'
                      }
                  };
              }).uniqBy(function (item) {
                  return item.place.address;
              }).value();

              var zones =  _.filter(tarifZones, function (place) {
                  return place.place.address.indexOf(val) >=0;
              }).map(function (place) {
                  place.place.type = 'Тарифная зона';
                  return place;
              });
              return _.concat(zones, featureMember);
          });
      };
    }
})();