'use strict';

angular.module('mean.bars').controller('ContactsController', ['$scope', '$stateParams', '$location', 'Global', 'Contacts', 'MeanUser', 'Circles',
  function($scope, $stateParams, $location, Global, Contacts, MeanUser, Circles) {
    $scope.global = Global;

    $scope.hasAuthorization = function(contact) {
      if (!contact || !contact.user) return false;
      return MeanUser.isAdmin || contact.user._id === MeanUser.user._id;
    };

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
    });

    $scope.create = function(isValid) {
      if (isValid) {
      //  $scope.contact.permissions = $scope.availableCircles.slice(2);
        var contact = new Contacts($scope.contact);
        console.log('Contacts', Contacts);
        contact.user = MeanUser.user;
        contact.acl = MeanUser.acl;
        contact.$save(function(response) {
          $location.path('contacts/' + response._id);
        });

        $scope.contact = {};

      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(contact) {
      if (contact) {
        contact.$remove(function(response) {
          for (var i in $scope.contacts) {
            if ($scope.contacts[i] === contact) {
              $scope.contacts.splice(i, 1);
            }
          }
          $location.path('contacts');
        });
      } else {
        $scope.contact.$remove(function(response) {
          $location.path('contacts');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var contact = $scope.contact;
        if (!contact.updated) {
          contact.updated = [];
        }
        contact.updated.push(new Date().getTime());
        contact.user = MeanUser.user;
        contact.acl = MeanUser.acl;
        contact.$update(function() {
          $location.path('contacts/' + contact._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Contacts.query(function(contacts) {
        console.log('contacts ', contacts);
        $scope.contacts = contacts;
      });
    };

    $scope.findOne = function() {
      Contacts.get({
        contactId: $stateParams.contactId
      }, function(contact) {
        console.log('findOne Contact ', contact);
        $scope.contact = contact;
      });
    };
  }
]);