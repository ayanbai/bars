'use strict';
/*global angular*/
angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
var templateUrl = '/bars/views/clients/';
    // states for my app
    $stateProvider
      .state('all clients', {
        url: '/clients',
        templateUrl: templateUrl+'list.html'
      })
      .state('create client', {
        url: '/clients/create',
        templateUrl: templateUrl+'create.html'
      })
      .state('edit client', {
        url: '/clients/:clientId/edit',
        templateUrl: templateUrl+'edit.html'
      })
      .state('client by id', {
        url: '/clients/:clientId',
        templateUrl: templateUrl+'view.html'
      });
  }
]);
