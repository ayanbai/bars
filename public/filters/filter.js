'use strict';
/*global angular*/

angular.module('mean.bars').filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          if (props.hasOwnProperty(prop) && item[prop] !== null) {
            var text = props[prop].toString().toLowerCase();
            if (text.length > 1) {
              if (item[prop] || item[prop] !== null) {
                if (item[prop]) {
                  if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                    itemMatches = true;
                    break;
                  }
                }
              }
            }
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    }
    else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});