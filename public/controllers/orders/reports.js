'use strict';
/*global angular, moment, _ */

angular.module('mean.bars').controller('ReportsController', ['$scope', 'MeanUser', 'Circles', '$q', 'Orders',
    function($scope, MeanUser, Circles, $q, Orders) {

        $scope.hasAuthorization = function(order) {
            if (!order || !order.user) return false;
            return MeanUser.isAdmin || order.user._id === MeanUser.user._id;
        };

        $scope.initCircles = function() {
            Circles.mine(function(acl) {
                console.log('initCircles', acl.allowed);
                $scope.availableCircles = acl.allowed;
                $scope.allDescendants = acl.descendants;
            });
        };

        $scope.orders = [];
        
        $scope.initCircles();

        $scope.getOrders = function() {
           Orders.query({}, function(orders) {
              console.log('orders', orders);
              $scope.orders = orders;
            });
        };
    }
]);