'use strict';
/*global angular, _ */

angular.module('mean.bars').controller('OrderCreateController', 
    ['$scope', '$location', 'MeanUser', 'Circles', '$q', 'Orders',  'orderResources', 'yandexLocations',
    function($scope, $location, MeanUser, Circles, $q, Orders, orderResources, yandexLocations) {
        $scope.passengersSaved = [];
        var Contacts = null;
        var Places = null;
        var individualClient = 'Физическое лицо';

        var initForm = function() {
            console.log('init Form');
            Circles.mine(function(acl) {
                $scope.availableCircles = acl.allowed;
                $scope.allDescendants = acl.descendants;
            });

            $scope.order = {
                direction: {
                    from: null,
                    to: null
                },
                moment: {
                    preorder: null,
                    start: null,
                    notification: null,
                    inplace: null,
                    met: null,
                    end: null,
                    closed: null
                },
                preorder: false,
                isCash: false,
                km: 0,
                comment: null,
                driver: null,
                passengers: null,
                status: $scope.statusCollection['ACCEPTED'],
                client: null,
                sum: 0,
                ticket: 0,
                service: null
            };
            $scope.$watch('order.preorder', function(newVal, oldVal) {
                var order = $scope.order;
                if (newVal === true) {
                    order.moment.preorder = new Date();
                    order.moment.preorder.setSeconds(0);
                    order.moment.preorder.setMilliseconds(0);
                }
                else {
                    delete order.moment.preorder;
                }
            });
        };

        $scope.contactTagTransform = orderResources.contacts.transform;
        $scope.addressTagTransform = orderResources.places.transform;
        $scope.onSelectPassenger = function(select) {
            var res = orderResources.contacts.onSelect(select);
            res.then(function(data) {
                $scope.passengersSaved.push(data);
                $scope.contacts.push(data);
            }, function(data) {
                console.log('Error on save contact');
            });
        };
        $scope.getLocations = function(val) {
            return yandexLocations(val, $scope.order.client.places);
        }

        $scope.onSelectPlace = function(select) {
            var res = orderResources.places.onSelect(select);

            res.then(function(data) {
                select.selected = data;
            }, function(data) {
                console.log('Error on select or add place', data);
            });
        };
        $scope.loadResources = function(state) {
            var deferred = $q.defer();
            orderResources.statuses.then(setStatus, errorCallback('statuses')).then(function() {
                orderResources.places.data.then(successPlacesCallback, errorCallback('places')).then(function() {
                    orderResources.contacts.data.then(successContactsCallback, errorCallback('contacts')).then(function() {
                        orderResources.drivers.then(successCallback('drivers'), errorCallback('drivers')).then(function() {
                            orderResources.clients.then(successClientsCallback, errorCallback('clients')).then(function() {
                                deferred.resolve();
                                initForm();
                            });
                        });
                    });
                });
            });

            function successPlacesCallback(result) {
                $scope.places = result;
                Places = orderResources.places.model;
            }

            function successContactsCallback(result) {
                $scope.contacts = result;
                Contacts = orderResources.contacts.model;
            }

            function successClientsCallback(result) {
             $scope.clients = _.map(result, function(client){
                if(client.name === null && client.title === null)
                {
                    client.title = individualClient;
                    client.name = '';
                }else {
                    client.company = 'Клиенты по без налу'
                }
                return client;
            });
            }

            function setStatus(statuses) {
                $scope.statuses = statuses;
                $scope.statusKeys = _.map(statuses, function(c) {
                    return c.key;
                });
                $scope.statusCollection = _.reduce(statuses, function(prev, next) {
                    prev[next.key] = next;
                    return prev;
                }, {});
            }

            function successCallback(name) {
                return function(data) {
                    $scope[name] = data;
                };
            }

            function errorCallback(name) {
                return function(error) {
                    console.log('Error at ' + name + ' text: ' + error);
                    deferred.reject();
                };
            }

            return deferred.promise;
        };
        $scope.hasAuthorization = function(order) {
            if (!order || !order.user) return false;
            return MeanUser.isAdmin || order.user._id === MeanUser.user._id;
        };
        $scope.create = function() {

            if ($scope.order.hasOwnProperty('driver') === true && $scope.order.driver !== null) {
                if ($scope.order.hasOwnProperty('status') && $scope.order.status.key === 'ACCEPTED') {
                    $scope.order.status = $scope.statusCollection['ASSIGNED'];
                }
            }

            $scope.order.isCash = $scope.order.client.name === null && $scope.order.client.title === individualClient;

            if ($scope.order.preorder === false) {
                delete $scope.order.moment.preorder;
                delete $scope.order.moment.start;
            }
/*
            $scope.order.direction = {
                from: $scope.order.direction[0],
                to: $scope.order.direction[1] || null
            };
            */

            $scope.order.passengers = $scope.passengersSaved;

            orderResources.places.save($scope.order.direction.from || null).then(function(place) {
                $scope.order.direction.from = place || $scope.order.direction.from;
                orderResources.places.save($scope.order.direction.to || null).then(function(place) {
                    $scope.order.direction.to = place || $scope.order.direction.to;
                    var order = new Orders($scope.order);
                    order.created = new Date();
                    order.user = MeanUser.user;
                    order.acl = MeanUser.acl;

                    console.log('order', $scope.order.direction);
                    order.$save(function(response) {
                        console.log('save result', response);
                        $location.path('orders');
                    });
                });
            });
        };
    }
]);