(function() {
  'use strict';

  /* jshint -W098 */
  angular
    .module('mean.bars')
    .controller('ReportsController', BarsController);

  BarsController.$inject = ['$scope', '$http', 'Global', 'Bars'];

  function BarsController($scope, $http, Global, Bars) {
    $scope.global = Global;
    $scope.package = {
      name: 'bars'
    };

    $scope.orders = [];
    
    $scope.getter = $http;

    $scope.getOrders = function() {
      
    };

  }
})();