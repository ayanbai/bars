
'use strict';
/*global angular*/

angular.module('mean.bars').factory('Clients', ['$resource',
  function($resource) {
    return $resource('api/clients/:clientId', { clientId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Contacts', ['$resource',
  function($resource) {
    return $resource('api/contacts/:contactId', { contactId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Drivers', ['$resource',
  function($resource) {
    return $resource('api/drivers/:driverId', { driverId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Cars', ['$resource',
  function($resource) {
    return $resource('api/cars/:carId', { carId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('PaymentTypes', ['$resource',
  function($resource) {
    return $resource('api/paymentTypes/:paymentTypeId', { paymentTypeId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Places', ['$resource',
  function($resource) {
    return $resource('api/places/:placeId', { placeId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Statuses', ['$resource',
  function($resource) {
    return $resource('api/statuses/:statusId', { statusId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);

angular.module('mean.bars').factory('Orders', ['$resource',
  function($resource) {
    console.log('Orders resources');
    return $resource('api/orders/:orderId', { orderId: '@_id' },
    {
      'update':  { method: 'PUT' }
    });
}]);
