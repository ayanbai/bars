'use strict';
/*global angular*/
angular.module('mean.bars').config(['$stateProvider',
  function($stateProvider) {
var templateUrl = '/bars/views/places/';
    // states for my app
    $stateProvider
      .state('all places', {
        url: '/places',
        templateUrl: templateUrl+'list.html'
      })
      .state('create place', {
        url: '/places/create',
        templateUrl: templateUrl+'create.html'
      })
      .state('edit place', {
        url: '/places/:placeId/edit',
        templateUrl: templateUrl+'edit.html'
      })
      .state('place by id', {
        url: '/places/:placeId',
        templateUrl: templateUrl+'view.html'
      });
  }
]);
